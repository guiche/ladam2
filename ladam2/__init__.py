
import sys

try:
    import pygapp
except:
    import os
    for path in '../pygapp_pyg', '../pygapp':
        if os.path.isdir(path):
            sys.path.append(path)
            break
    else:
        print("le paquet python pygapp est requis - veuillez l'installer")

import pygapp.jeu as jeu
import pygapp.media
pygapp.media.setupResourcePath(__file__) 
import ladam2.app

pygapp.jeu.CLASSE_PARTIE = ladam2.app.Application

#import pygapp.sauvegarde
#pygapp.sauvegarde.LOAD_SUBSTITUTION = {}
import sys

def main():
    if len(sys.argv) > 1:
        ladam2.app.INTERVALLE = int(sys.argv[1])
    jeu.BoucleJeu()

#this calls the 'main' function when this script is executed
if __name__ == '__main__': 
    main()
