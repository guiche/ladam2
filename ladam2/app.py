'''
Created on 7 Jan 2015

@author: Apsara
'''
import pygapp.menus
from pygapp import app
from pygapp import media
import pygame
import os

INTENSITES = [pygame.Color(i,i,i) for i in xrange(0,256)]

INTERVALLE = 2

def listeImagesTiff(sous_dossier):
    ''' liste des fichiers svg dans le repertoire d'images '''
    return pygapp.sauvegarde.ListeRepertoire(os.path.join(pygapp.media.IMAGE_REP,sous_dossier), 'tif', inclureDossiers=False, coupeSuffixe=False)

def listeVideos():
    ''' liste des fichiers svg dans le repertoire d'images '''
    for filename in os.listdir(pygapp.media.IMAGE_REP):
        if os.path.isdir(os.path.join(pygapp.media.IMAGE_REP,filename)):
            yield filename


class Video:
    
    def __init__(self, repertoire, taille_pixel):
        self.repertoire = repertoire
        self.noms_image = listeImagesTiff(repertoire)
        self.image_fond = media.charge_image(os.path.join(repertoire,'fond.tif'))
        self.noms_image = [img for img in listeImagesTiff(repertoire) if not img.startswith('fond')]    
        self.images_surf = [media.charge_image(os.path.join(repertoire,nomFichier), taille_pixel) for nomFichier in self.noms_image]
        self.index_image = 0
        self.tailleX = self.images_surf[0].get_width()
            
    def __str__(self):
        return self.repertoire

    
    def affiche(self, surf, pos):
        surf.blit(self.image_fond, pos)
        surf.blit(self.images_surf[self.index_image], pos)


    def incremente(self):
        self.index_image += 1
        self.index_image %= len(self.noms_image)

    def nomImageCourante(self):
        return self.repertoire + '/' + self.noms_image[self.index_image]

class Application(app.Application):
    '''
    classdocs
    '''
        
    def __init__(self, vitesse=5.):

        self.taille_pixel = 25 # taille d'un macro pixel, en pixel
        self.vitesse_defilement = vitesse # en pixels par secondes
        self.intervalle_defilement = INTERVALLE
        
        self.marge_hauteur = 100
        
        DimEcran = 50*self.taille_pixel, 5*self.taille_pixel + self.marge_hauteur
        app.Application.__init__(self,DimEcran)
        #self.image_echelle = self.taille_pixel
        self.sens_defilement = -1 # +1 : gauche vers la droite, -1 droite vers la gauche
        
        
    def __str__(self):
        return self.nom
    

    def InitPreBoucle(self):
        
        app.Application.InitPreBoucle(self)
        
        # icone d'application
        surf = pygame.Surface((32,32))
        surf.fill((0,0,0))
        pygame.draw.circle(surf, (255,255,255), (8,6), 3)
        pygame.draw.circle(surf, (255,255,255), (11,18), 3)
        pygame.draw.circle(surf, (255,255,255), (22,22), 3)
        pygame.display.set_icon(surf)
        
        pygame.display.set_caption('Ladam2')
        
        self.videos = []
        for repertoire in listeVideos():
            
            self.videos.append(Video(repertoire, self.taille_pixel))
                    
        self.videos_defilantes = []
        self.video_index = 0
        self.accum_dx = 0
        
    @property
    def video_index(self):
        """ index dans la liste de toutes les images de la prochaine image a paraitre"""
        return self._video_index
    
    @video_index.setter
    def video_index(self, val):
        self._video_index = val
        self._video_index %= len(self.videos)
        
    def MajTourFuncIter(self,deltaT,tempsT):
        
        self.accum_dx += self.vitesse_defilement * deltaT
        
        # arrondi entier pour garantir la progression de pixel en pixel
        dX = round(self.accum_dx)

        # ajout de nouvelles images a gauche
        if not self.videos_defilantes or ((self.sens_defilement > 0 and \
            self.videos_defilantes[0][0] > self.intervalle_defilement) or (self.sens_defilement < 0 and self.videos_defilantes[0][0] + self.videos_defilantes[0][-1].tailleX < self.DimEcran[0] - self.intervalle_defilement*self.taille_pixel)):
            video_nov = self.videos[self.video_index]
            self.video_index += 1
            if self.sens_defilement > 0:
                self.videos_defilantes.insert(0,[-video_nov.tailleX / self.taille_pixel, video_nov])
            else:
                self.videos_defilantes.insert(0,[(self.DimEcran[0]+video_nov.tailleX) / self.taille_pixel, video_nov])
        
        if dX > 0:
                    
            self.accum_dx -= dX
            
            for _posX, video in self.videos_defilantes:
                video.incremente()
            
            # mise a jour de l'abscisse de chacune des images
            for pos_img in reversed(self.videos_defilantes):
                pos_img[0] += dX * self.sens_defilement

            # suppression de l'image en fin de course a droite
            derX, image_der = self.videos_defilantes[-1]
            pop = False
            if self.sens_defilement > 0: 
                if derX > self.DimEcran[0]/self.taille_pixel:
                    pop = True
            else:
                if derX < -image_der.tailleX/self.taille_pixel:
                    pop = True
            if pop:
                self.videos_defilantes[-1][1].index_image = 0
                self.videos_defilantes.pop()
                
        yield lambda : None
  
    def PeintUneToile(self, deltaT):
        """rendu graphique"""
        app.Application.PeintUneToile(self, deltaT)

        for posX, video in self.videos_defilantes:
            pos = (posX*self.taille_pixel,self.marge_hauteur/2)
            video.affiche(self._Surf,pos)
            if not self.Courir and posX > 0:
                self.afficheParatexte(str(video), (posX*self.taille_pixel,-self.marge_hauteur/4))

    
    def Grossissement(self,*args,**kwargs):
        pass  
        
champs_visibles = 'vitesse_defilement', 'intervalle_defilement', 'video_index'
setattr(Application, "_champs_description_", champs_visibles)
