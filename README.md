# Ladam2 #


### Dépendences : 
- python 2.7
- pygame :
http://pygame.org/download.shtml

- Installer pygapp depuis bitbucket :


```
#!shell

git clone https://guiche@bitbucket.org/guiche/pygapp.git
```

cloner le dépot au même niveau que le répertoire ladam2.
Passer sur la branche : **pygame_trans_dev**

### Lancement :

```
#!python

python lancer.py
```
### Contrôles :
- <espace> pour pauser / dépauser
- <echap> pour quiter
- <entrer> pour accéder au menu d'édition des paramètres 
-- le nom des paramètres est explicite.